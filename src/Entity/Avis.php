<?php

namespace App\Entity;

use App\Repository\AvisRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=AvisRepository::class)
 */
class Avis
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="date")
     */
    private $date_avis;

    /**
     * @ORM\Column(type="string", length=1)
     */
    private $note_avis;

    /**
     * @ORM\Column(type="text")
     */
    private $description_avis;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $moderation_avis;

    /**
     * @ORM\ManyToOne(targetEntity=Game::class, inversedBy="avis")
     * @ORM\JoinColumn(nullable=false)
     */
    private $avisGame;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="avis")
     * @ORM\JoinColumn(nullable=false)
     */
    private $avisUser;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDateAvis(): ?\DateTimeInterface
    {
        return $this->date_avis;
    }

    public function setDateAvis(\DateTimeInterface $date_avis): self
    {
        $this->date_avis = $date_avis;

        return $this;
    }

    public function getNoteAvis(): ?string
    {
        return $this->note_avis;
    }

    public function setNoteAvis(string $note_avis): self
    {
        $this->note_avis = $note_avis;

        return $this;
    }

    public function getDescriptionAvis(): ?string
    {
        return $this->description_avis;
    }

    public function setDescriptionAvis(string $description_avis): self
    {
        $this->description_avis = $description_avis;

        return $this;
    }

    public function getModerationAvis(): ?string
    {
        return $this->moderation_avis;
    }

    public function setModerationAvis(string $moderation_avis): self
    {
        $this->moderation_avis = $moderation_avis;

        return $this;
    }

    public function getAvisGame(): ?Game
    {
        return $this->avisGame;
    }

    public function setAvisGame(?Game $avisGame): self
    {
        $this->avisGame = $avisGame;

        return $this;
    }

    public function getAvisUser(): ?User
    {
        return $this->avisUser;
    }

    public function setAvisUser(?User $avisUser): self
    {
        $this->avisUser = $avisUser;

        return $this;
    }
}

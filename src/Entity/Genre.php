<?php

namespace App\Entity;

use App\Repository\GenreRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=GenreRepository::class)
 */
class Genre
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $title_genre;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $description_genre;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $image_genre;

    /**
     * @ORM\ManyToMany(targetEntity=Game::class, mappedBy="genre")
     */
    private $games;

    public function __construct()
    {
        $this->games = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitleGenre(): ?string
    {
        return $this->title_genre;
    }

    public function setTitleGenre(string $title_genre): self
    {
        $this->title_genre = $title_genre;

        return $this;
    }

    public function getDescriptionGenre(): ?string
    {
        return $this->description_genre;
    }

    public function setDescriptionGenre(?string $description_genre): self
    {
        $this->description_genre = $description_genre;

        return $this;
    }

    public function getImageGenre(): ?string
    {
        return $this->image_genre;
    }

    public function setImageGenre(?string $image_genre): self
    {
        $this->image_genre = $image_genre;

        return $this;
    }

    /**
     * @return Collection|Game[]
     */
    public function getGames(): Collection
    {
        return $this->games;
    }

    public function addGame(Game $game): self
    {
        if (!$this->games->contains($game)) {
            $this->games[] = $game;
            $game->addGenre($this);
        }

        return $this;
    }

    public function removeGame(Game $game): self
    {
        if ($this->games->removeElement($game)) {
            $game->removeGenre($this);
        }

        return $this;
    }
}

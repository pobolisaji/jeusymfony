<?php

namespace App\Entity;

use App\Repository\GameRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=GameRepository::class)
 */
class Game
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $title_game;

    /**
     * @ORM\Column(type="text")
     */
    private $description_game;

    /**
     * @ORM\Column(type="date")
     */
    private $date_game;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $image_game;

    /**
     * @ORM\ManyToMany(targetEntity=Genre::class, inversedBy="games")
     */
    private $genre;

    /**
     * @ORM\ManyToMany(targetEntity=User::class, inversedBy="games")
     */
    private $user;

    /**
     * @ORM\OneToMany(targetEntity=Avis::class, mappedBy="avisGame", orphanRemoval=true)
     */
    private $avis;

    public function __construct()
    {
        $this->genre = new ArrayCollection();
        $this->user = new ArrayCollection();
        $this->avis = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitleGame(): ?string
    {
        return $this->title_game;
    }

    public function setTitleGame(string $title_game): self
    {
        $this->title_game = $title_game;

        return $this;
    }

    public function getDescriptionGame(): ?string
    {
        return $this->description_game;
    }

    public function setDescriptionGame(string $description_game): self
    {
        $this->description_game = $description_game;

        return $this;
    }

    public function getDateGame(): ?\DateTimeInterface
    {
        return $this->date_game;
    }

    public function setDateGame(\DateTimeInterface $date_game): self
    {
        $this->date_game = $date_game;

        return $this;
    }

    public function getImageGame(): ?string
    {
        return $this->image_game;
    }

    public function setImageGame(?string $image_game): self
    {
        $this->image_game = $image_game;

        return $this;
    }

    /**
     * @return Collection|Genre[]
     */
    public function getGenre(): Collection
    {
        return $this->genre;
    }

    public function addGenre(Genre $genre): self
    {
        if (!$this->genre->contains($genre)) {
            $this->genre[] = $genre;
        }

        return $this;
    }

    public function removeGenre(Genre $genre): self
    {
        $this->genre->removeElement($genre);

        return $this;
    }

    /**
     * @return Collection|User[]
     */
    public function getUser(): Collection
    {
        return $this->user;
    }

    public function addUser(User $user): self
    {
        if (!$this->user->contains($user)) {
            $this->user[] = $user;
        }

        return $this;
    }

    public function removeUser(User $user): self
    {
        $this->user->removeElement($user);

        return $this;
    }

    /**
     * @return Collection|Avis[]
     */
    public function getAvis(): Collection
    {
        return $this->avis;
    }

    public function addAvi(Avis $avi): self
    {
        if (!$this->avis->contains($avi)) {
            $this->avis[] = $avi;
            $avi->setAvisGame($this);
        }

        return $this;
    }

    public function removeAvi(Avis $avi): self
    {
        if ($this->avis->removeElement($avi)) {
            // set the owning side to null (unless already changed)
            if ($avi->getAvisGame() === $this) {
                $avi->setAvisGame(null);
            }
        }

        return $this;
    }
}
